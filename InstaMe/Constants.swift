//
//  Constants.swift
//  InstaMe
//
//  Created by Romeo Violini on 30/08/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct Instagram {
        
        static let InstagramAuthUrl = "https://api.instagram.com/oauth/authorize/"
        static let InstagramApiUrl  = "https://api.instagram.com/v1/users/"
        static let InstagramClientID  = "061448a1df8b441380bc9c6862aa2eb9"
        static let InstagramClientSecret = "f273a0741e064a5798a6ecb564ef8efa"
        static let InstagramRedirectUri = "http://www.romeoviolini.com"
        static let InstagramScope = ""
    }
    
    struct Segues {
        
        static let LoginSegue = "TryFirstLogin"
        static let GoToGallery = "goToGallery"
        static let OpenZoomController = "openZoomController"
    }
    
    struct Resources {
        
        static let GenericThumbImageName = "imageNotFound"
    }
    
    struct Cache {
        
        static let Limit = 25
    }
    
    struct Cookie {
        
        static let InstagramDomain = "www.instagram.com"
        static let APIInstagramDomain = "api.instagram.com"
    }
    
    struct ScrollView {
        
        static let MinimumZoom: CGFloat = 1.0
        static let MaximumZoom: CGFloat = 6.0
    }
    
    struct Cell {
        
        struct Xib {
            
            static let GalleryCell = "GalleryCell"
        }
        
        struct Identifier {
            
            static let GalleryCell = "Cell"
        }
    }
}
