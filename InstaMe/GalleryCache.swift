//
//  GalleryCache.swift
//  InstaMe
//
//  Created by Romeo Violini on 05/09/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import Foundation
import UIKit

class GalleryCache {
    
    private var picturesDictionary: NSCache<AnyObject, AnyObject>!
    
    //MARK: Instance
    static let instance : GalleryCache = {
        return GalleryCache()
    }()
    
    init() {
        //Initialize cache
        self.picturesDictionary = NSCache()
        self.picturesDictionary.countLimit = Constants.Cache.Limit
    }
    
    static func setPicture(picture: UIImage, withId id: String) {
        //Set picture in cache
        GalleryCache.instance.picturesDictionary.setObject(picture, forKey: id as AnyObject)
    }
    
    static func getPictureForId(id: String) -> AnyObject? {
        //Try to picture in cache
        if let picture = GalleryCache.instance.picturesDictionary.object(forKey: id as AnyObject) {
            return picture
        } else {
            return nil
        }
    }
    
    static func clearCache() {
        //Clear Cache
        GalleryCache.instance.picturesDictionary.removeAllObjects()
    }
}
