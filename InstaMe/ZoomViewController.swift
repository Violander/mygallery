//
//  ZoomViewController.swift
//  InstaMe
//
//  Created by Romeo Violini on 06/09/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class ZoomViewController: UIViewController, UIScrollViewDelegate {
    
    var image: UIImage?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set zoom image
        self.imageView.image = image
        
        //Set ZoomScale of scrollview
        self.scrollView.minimumZoomScale = Constants.ScrollView.MinimumZoom
        self.scrollView.maximumZoomScale = Constants.ScrollView.MaximumZoom
        
        //Add doubleTap to manage quickly the zoom
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(onDoubleTap))
        doubleTap.numberOfTapsRequired = 2
        self.scrollView.addGestureRecognizer(doubleTap)
    }
    
    //MARK: - ScrollViewDelegate
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    //MARK: - User Interaction
    func onDoubleTap() {
        //Decide if zoom-in or zoom-out
        if self.scrollView.zoomScale > Constants.ScrollView.MinimumZoom {
            self.scrollView.setZoomScale(Constants.ScrollView.MinimumZoom, animated: true)
        } else {
            self.scrollView.setZoomScale(Constants.ScrollView.MaximumZoom/2, animated: true)
        }
    }
    
    @IBAction func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
