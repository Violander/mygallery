//
//  LoginViewController.swift
//  InstaMe
//
//  Created by Romeo Violini on 04/09/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    // MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //initialize the main layout of the view
        self.initializeLayout()
        
        //Try to login on instagram through webview
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token", arguments: [Constants.Instagram.InstagramAuthUrl,Constants.Instagram.InstagramClientID,Constants.Instagram.InstagramRedirectUri])
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        self.webView.loadRequest(urlRequest)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        let specialNavigationController = self.navigationController as? SpecialNavigationController
        //Set Transparent NavBar
        specialNavigationController?.setNavbarColorFromType(barType: .transparent, withBottomBorder: true)
        //Set black status bar
        specialNavigationController?.setCurrentStatusBarStyle(statusBarStyle: .default)
    }
    
    //MARK: - Manage Request
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        //Check for the accessToken
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.hasPrefix(Constants.Instagram.InstagramRedirectUri) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            //Manage the accessToken
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String)  {
        //Set accessToken to the main delegate
        AppDelegate.instance.instagramAccessToken = authToken
        //Go to Gallery View
        self.performSegue(withIdentifier: Constants.Segues.GoToGallery , sender: nil)
    }
    
    // MARK: - UIWebViewDelegate
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        //Check the request till the accesstoken
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.loadingIndicator.isHidden = false
        self.loadingIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.loadingIndicator.isHidden = true
        self.loadingIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.webViewDidFinishLoad(webView)
    }
    
    // MARK: - User Interaction
    func cancelLogin() {
        //Stop Loading
        self.webView.stopLoading()
        //Return to Menu
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Layout
    func initializeLayout() {
        //Set the backButton
        let backButton = UIBarButtonItem(title: "APP.CANCEL".localized(withComment: "Cancel"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelLogin));
        backButton.setTitleTextAttributes( [NSForegroundColorAttributeName : UIColor.black], for: UIControlState.normal)
        self.navigationItem.leftBarButtonItem = backButton
    }
}
