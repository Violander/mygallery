//
//  GalleryCell.swift
//  InstaMe
//
//  Created by Romeo Violini on 05/09/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var zoomUrl: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell() {
        self.layer.cornerRadius = 10.0
        self.layer.masksToBounds = true
    }
    
}
