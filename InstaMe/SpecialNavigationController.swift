//
//  SpecialNavigationController.swift
//  InstaMe
//
//  Created by Romeo Violini on 04/09/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

enum SpecialBarType {
    case light,black,transparent
}

import UIKit

class SpecialNavigationController: UINavigationController {
   
    var currentStatusBarStyle : UIStatusBarStyle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //Set Navigation Bar color by type
    func setNavbarColorFromType(barType: SpecialBarType, withBottomBorder: Bool = false) {
        var image: UIImage?
        var type: UIBarStyle = .default
        var isTranslucent = false
        
        switch barType {
        case .black:
            type = .black
            break
        case .transparent:
            type = .default
            image = UIImage()
            isTranslucent = true
            break
        default:
            break
        }
        self.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        if withBottomBorder {
           self.navigationBar.shadowImage = UIImage.imageWithColor(color: UIColor.black, size: CGSize.init(width: 1.0, height: 0.5))
        } else {
           self.navigationBar.shadowImage = image
        }
        
        self.navigationBar.isTranslucent = isTranslucent
        self.navigationBar.barStyle = type
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if self.currentStatusBarStyle != nil {
            return self.currentStatusBarStyle!
        } else {
            return Layout.StatusBar.Style
        }
    }
    
    
    func setCurrentStatusBarStyle (statusBarStyle : UIStatusBarStyle) {
        self.currentStatusBarStyle = statusBarStyle
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
