//
//  GalleryViewController.swift
//  InstaMe
//
//  Created by Romeo Violini on 05/09/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GalleryViewController: UIViewController, UICollectionViewDelegateFlowLayout  {
    
    var maxId: String?
    var currentImageZoom: UIImage?
    var isLoadingContent: Bool = false
    
    var pictures : [Picture]!
    var varPictures: Variable<[Picture]>!
    private let disposeBag = DisposeBag()
    
    var refreshControl: UIRefreshControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewLoading: UIView!
    
    // MARK: - View
    override func loadView() {
        super.loadView()
        
        //Register xib cell
        self.collectionView.register(UINib(nibName: Constants.Cell.Xib.GalleryCell, bundle: nil) , forCellWithReuseIdentifier: Constants.Cell.Identifier.GalleryCell)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Initialize the main layout of the view
        self.initializeLayout()
        
        //Initialize the refreshControl to enable refresh on CollectionView
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        self.collectionView!.addSubview(self.refreshControl)
        
        //Initialize DataSource
        self.pictures = []
        self.varPictures = Variable<[Picture]>([])
        self.collectionView.rx.setDelegate(self).addDisposableTo(disposeBag)
        
        //RX bind dataSource to collectionview
        self.varPictures.asObservable()
            .bindTo(self.collectionView.rx.items(cellIdentifier: Constants.Cell.Identifier.GalleryCell, cellType: GalleryCell.self)) { row, picture, cell in
                
                //Configure Cell
                cell.configureCell()
                //Subscribe to the download image task of the picture
                let _ = picture.getThumb().subscribe(
                    
                    onNext: { image in
                        //Update the cell with image downloaded
                        DispatchQueue.main.async {
                            cell.loadingIndicator.stopAnimating()
                            cell.imageView.image = image
                        }
                })
            }
            .addDisposableTo(self.disposeBag)
        
        //RX Subscribe collectionView to item selection
        self.collectionView.rx.itemSelected
            .subscribe(
                onNext: { indexPath in
                    //Remove LoadingView
                    self.viewLoading.isHidden = false
                    //Get Picture from dataSource
                    let picture = self.pictures[indexPath.row]
                    //Check if picture is downloaded
                    if picture.isDownloaded {
                        //Subscribe to the download image task of the picture
                        let _ = picture.getZoom().subscribe(
                            onNext: { image in
                                //Set the current image to zoom
                                self.currentImageZoom = image
                                //Go to ZoomView
                                DispatchQueue.main.async {
                                    self.performSegue(withIdentifier: Constants.Segues.OpenZoomController, sender: nil)
                                }
                                
                        },
                            onError: { error in
                                self.alert(message: "ERROR.ZOOM.MESSAGE".localized(withComment: "Failed to load zoom of picture"), title: "ERROR.ZOOM.TITLE".localized(withComment: "Zoom Error"))
                                DispatchQueue.main.async {
                                    self.viewLoading.isHidden = true
                                }
                        })
                    } else {
                        self.alert(message: "ERROR.ZOOM.MESSAGE".localized(withComment: "Failed to load zoom of picture"), title: "ERROR.ZOOM.TITLE".localized(withComment: "Zoom Error"))
                        DispatchQueue.main.async {
                            self.viewLoading.isHidden = true
                        }
                    }
            })
            .addDisposableTo(disposeBag)
        
        //Get the json from the recent media erlier to maxId
        self.getRecentPictures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Check if there are already pictures
        if self.pictures.count > 0 {
            //Hide or not the loading view
            self.viewLoading.isHidden = true
        }
        let specialNavigationController = self.navigationController as? SpecialNavigationController
        //Set Transparent NavBar
        specialNavigationController?.setNavbarColorFromType(barType: .transparent, withBottomBorder: true)
        //Set black status bar
        specialNavigationController?.setCurrentStatusBarStyle(statusBarStyle: .default)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        //Invalidate the layout of collectionView when application scroll to reload correct layout
        self.collectionView!.collectionViewLayout.invalidateLayout()
    }
    
    // MARK: - Gallery
    func getRecentPictures() {
        //Check if has laready loaded content
        if !self.isLoadingContent {
            self.isLoadingContent = true
            
            var accessTokenQuery = AppDelegate.instance.instagramAccessToken!
            
            //If exists a maxId load posts earlier than it
            if self.maxId != nil {
                accessTokenQuery += "&max_id=\(self.maxId!)"
            }
            //Request to get recent media user
            let req = URLRequest(url: URL(string: "\(Constants.Instagram.InstagramApiUrl)self/media/recent/?access_token=\(accessTokenQuery)")!)
            //Rx Subscribe to request to waiting for json
            let _ = URLSession.shared.rx.json(request: req)
                .subscribe(onNext: { json in
                    //Map Json to dictionary
                    let jsonDictionary = json as! Dictionary<String, AnyObject>
                    let arrPictures = jsonDictionary["data"] as! [Dictionary<String, AnyObject>]
                    //Update Gallery by Dictionary
                    self.updatePicturesByJSON(picArrDict: arrPictures)
                    //Check Pagination if there are earlier media contents
                    if let dicPagination = jsonDictionary["pagination"] as? Dictionary<String, AnyObject> {
                        //Try set the maxId from pagination
                        if let max = dicPagination["next_max_id"] as? String {
                            //Check if maxId is the same of new max id, because if it's the same stop loading content
                            self.maxId = self.maxId != max ? max : nil
                        } else {
                            self.maxId = nil
                        }
                    } else {
                        self.maxId = nil
                    }
                }, onError: { error in
                    self.viewLoading.isHidden = true
                    self.alert(message: "ERROR.IMAGES.MESSAGE".localized(withComment: "Failed to get pictures from Instagram"), title: "ERROR.IMAGES.TITLE".localized(withComment: "Instagram Error"))
                    
                })
        }
    }
    
    private func updatePicturesByJSON(picArrDict: [Dictionary<String, AnyObject>]) {
        //Iterate dictionary and create Picture for gallery
        for picDict in picArrDict {
            let thumb = picDict["images"]?["thumbnail"] as! Dictionary<String, Any>
            let zoom = picDict["images"]?["standard_resolution"] as! Dictionary<String, Any>
            let pic = Picture(id: picDict["id"] as! String, thumbUrl: thumb["url"] as! String, zoomUrl: zoom["url"] as! String)
            if !self.viewLoading.isHidden {
                DispatchQueue.main.async {
                    self.viewLoading.isHidden = true
                }
            }
            self.pictures.append(pic)
            self.varPictures.value = self.pictures
        }
        self.isLoadingContent = false
    }
    
    // MARK: - UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //Check if exist a pagination for instagram media
        if self.maxId != nil {
            //Check if user has reached the bottom of collectionview
            if (scrollView.bounds.maxY == scrollView.contentSize.height) {
                //Load More Media
                self.getRecentPictures()
            }
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //Change the standard size of the collectionViewCell if device is iPad
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize.init(width: 150.0, height: 150.0)
        }
        
        return CGSize.init(width: 100.0, height: 100.0)
    }
    
    // MARK: - Layout
    func initializeLayout() {
        //Set the backButton
        let backButton = UIBarButtonItem(title: "APP.LOGOUT".localized(withComment: "Logout"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(logout));
        backButton.setTitleTextAttributes( [NSForegroundColorAttributeName : UIColor.black], for: UIControlState.normal)
        self.navigationItem.leftBarButtonItem = backButton
    }
    
    // MARK: - User Interaction
    func logout() {
        //Remove cookies to delete accessToken and logout user
        let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
        for cookie in cookieJar.cookies! as [HTTPCookie]{
            if cookie.domain == Constants.Cookie.InstagramDomain ||
                cookie.domain == Constants.Cookie.APIInstagramDomain {
                cookieJar.deleteCookie(cookie)
            }
        }
        let _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    func reloadData() {
        //Reset all parameters
        self.maxId = nil
        self.isLoadingContent = false
        self.pictures.removeAll()
        self.varPictures.value = self.pictures
        //Reload all media from the begin
        self.getRecentPictures()
        self.refreshControl.endRefreshing()
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Pass current zoom image to ZoomViewController
        if segue.identifier == Constants.Segues.OpenZoomController {
            let vc = segue.destination as! ZoomViewController
            vc.image = self.currentImageZoom
            self.currentImageZoom = nil
        }
    }
}
