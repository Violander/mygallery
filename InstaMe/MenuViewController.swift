//
//  MenuViewController.swift
//  InstaMe
//
//  Created by Romeo Violini on 30/08/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var viewBottom: UIView!
    
    var gradient: CAGradientLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initializeLayout()
    }

    // MARK: - View
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let specialNavigationController = self.navigationController as? SpecialNavigationController
        //Set Transparent NavBar
        specialNavigationController?.setNavbarColorFromType(barType: .transparent)
        //Set black status bar
        specialNavigationController?.setCurrentStatusBarStyle(statusBarStyle: .default)
    }
    
    // MARK: - Layout
    func initializeLayout() {
        
        self.viewBottom.layer.borderWidth = 1.0
        self.viewBottom.layer.borderColor = UIColor.black.cgColor
        self.viewBottom.layer.cornerRadius = 15.0
        self.viewBottom.layer.masksToBounds = true
    }
}
