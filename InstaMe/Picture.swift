//
//  GalleryPicture.swift
//  InstaMe
//
//  Created by Romeo Violini on 05/09/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

enum MyError: Error {
    case zoomErrorDownload
}

enum ImageType {
    case thumb
    case zoom
}

class Picture {
    
    var id: String?
    var thumbUrl: String?
    var zoomUrl: String?
    var isDownloaded: Bool
    
    //Initialization
    init(id: String, thumbUrl: String, zoomUrl: String) {
        self.id = id
        self.thumbUrl = thumbUrl
        self.zoomUrl = zoomUrl
        self.isDownloaded = false
    }
    
    //RX get thumb image observable
    func getThumb() -> Observable<UIImage> {
        return self.getImageFromType(imageType: .thumb)
    }
    
    //RX get zoom image observable
    func getZoom() -> Observable<UIImage> {
        return self.getImageFromType(imageType: .zoom)
    }
    
    //RX get image observable by type
    private func getImageFromType(imageType: ImageType) -> Observable<UIImage> {
        //Set the right url path
        var urlPath = self.thumbUrl!
        if imageType == ImageType.zoom {
            urlPath = self.zoomUrl!
        }
        //Create the request
        let request = URLRequest(url: URL(string: urlPath)!)
        //return the Observable creation
        return Observable.create { observer in
            //Set the right id of image (for caching)
            var idImage = self.id!
            if imageType == ImageType.zoom {
                idImage = "\(self.id!)-zoom"
            }
            //Check if image is in cache
            if let picture = GalleryCache.getPictureForId(id: idImage) {
                //Send to Observer the image
                self.isDownloaded = true
                observer.onNext(picture as! UIImage)
                return Disposables.create()
            }
            //Create the session and the task for the request and start task
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: request) { data, response, error in
                //Error case
                if error != nil {
                    //If image is zoom
                    if imageType == ImageType.zoom {
                        //Send Error
                        observer.onError(MyError.zoomErrorDownload)
                    } else {
                        //Send generic thumb of image
                        self.isDownloaded = false
                        observer.onNext(UIImage(named:Constants.Resources.GenericThumbImageName)!)
                    }
                } else {
                    if let HTTPResponse = response as? HTTPURLResponse {
                        //Check status code of the response
                        if 200 ..< 300 ~= HTTPResponse.statusCode {
                            if data != nil {
                                //Try to set the image downloaded
                                if let picture = UIImage(data: data!) {
                                    //Send to Observer the image
                                    if imageType == ImageType.thumb {
                                        self.isDownloaded = true
                                    }
                                    GalleryCache.setPicture(picture: picture, withId: idImage)
                                    observer.onNext(picture)
                                } else {
                                    //If failed to get image from downloaded data
                                    //If image is zoom
                                    if imageType == ImageType.zoom {
                                        //Send Error
                                        observer.onError(MyError.zoomErrorDownload)
                                    } else {
                                        //Send generic thumb of image
                                        self.isDownloaded = false
                                        observer.onNext(UIImage(named:Constants.Resources.GenericThumbImageName)!)
                                    }
                                }
                            } else {
                                //If data is nil
                                //If image is zoom
                                if imageType == ImageType.zoom {
                                    //Send Error
                                    observer.onError(MyError.zoomErrorDownload)
                                } else {
                                    //Send generic thumb of image
                                    self.isDownloaded = false
                                    observer.onNext(UIImage(named:Constants.Resources.GenericThumbImageName)!)
                                }
                            }
                        } else {
                            //If status code is different from allowed
                            //If image is zoom
                            if imageType == ImageType.zoom {
                                //Send Error
                                observer.onError(MyError.zoomErrorDownload)
                            } else {
                                //Send generic thumb of image
                                self.isDownloaded = false
                                observer.onNext(UIImage(named:Constants.Resources.GenericThumbImageName)!)
                            }
                        }
                    } else {
                        //If response is not HTTPResponse
                        //Send generic thumb of image
                        self.isDownloaded = false
                        observer.onNext(UIImage(named:Constants.Resources.GenericThumbImageName)!)
                    }
                }
            }
            //Start task
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
}
